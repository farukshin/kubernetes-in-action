Создание образа:

`docker build -t kubia .`

Запуск контейнера:

`docker run --name kubia-container -p 8080:8080 -d kubia`

Тегирование образа:

`docker tag kubia farukshin/kubia`

Push в Docker Hub:

`docker push farukshin/kubia`

Теперь образ можно запустить на любой машине:

`docker run -p 8080:8080 -d farukshin/kubia`

Запуск приложения в кластере kubernetes:

`kubectl run --image=farukshin/kubia --port=8080 --generator=run/v1 replicationcontroller "kubia" created`

Проверка запушенных pod'ов:

`kubectl get pods`

